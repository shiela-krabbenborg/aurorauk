import { Container } from 'react-bootstrap';
import './App.css';
import NavigationBar from './components/NavBar';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React from 'react';
import Homepage from './pages/Homepage';

function App() {
  return (
    <Router>
      <NavigationBar />
      <Routes>
        <Route path="/" element={<Homepage />} />
      </Routes>
    </Router>
  );
}

export default App;
