import React from 'react';
import { Navbar, Nav, Button, Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquarePhone } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from 'react-helmet';

import logo from '../assets/images/logo.png';

const NavigationBar = () => (
  <>
    <Helmet>
      <script
        src="https://assets.calendly.com/assets/external/widget.js"
        async
      ></script>
    </Helmet>
    <Navbar fixed="top" bg="transparent" expand="lg">
      <Container className="d-flex justify-content-between align-items-center">
        <Navbar.Brand href="#home">
          <img
            src={logo}
            width="160"
            height="50"
            className="d-inline-block align-top"
            alt="Logo"
          />
        </Navbar.Brand>
        <Button
          // variant="primary"
          className="book-call-button d-flex align-items-center"
          font-style={'AgendaLight'}
          onClick={() =>
            window.Calendly.initPopupWidget({
              url: 'https://calendly.com/aurorawellness/aurora-enquiry-call?hide_event_type_details=1',
            })
          }
        >
          <h5>
            BOOK A CALL
            <FontAwesomeIcon icon={faSquarePhone} className="icon-margin" />
          </h5>
        </Button>
      </Container>
    </Navbar>
  </>
);

export default NavigationBar;
