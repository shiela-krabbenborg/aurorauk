import React, { useState } from 'react';
import { Container, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSquarePhone,
  faAnglesDown,
  faArrowRightLong,
} from '@fortawesome/free-solid-svg-icons';

const Homepage = () => {
  return (
    <div className="hero">
      <div className="hero-background"></div>
      <Container className="hero-content">
        <h1>Unlock their Full Potential</h1>
        <h2>Employee Wellbeing Solutions that Work</h2>
        <Button className="book-call-button d-flex align-items-center">
          <h5>
            BOOK YOUR COMPLIMENTARY CONSULTATION
            <FontAwesomeIcon
              icon={faSquarePhone}
              className="icon-margin"
              size="1.5x"
            />
          </h5>
        </Button>
      </Container>
    </div>
  );
};

export default Homepage;
