import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Helmet } from 'react-helmet';

// import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <Helmet>
      <script
        src="https://assets.calendly.com/assets/external/widget.js"
        async
      ></script>
    </Helmet>
    <App />
  </React.StrictMode>
);
